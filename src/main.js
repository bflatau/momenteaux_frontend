import App from './App.svelte';
import Example from './pages/Example.svelte';

const app = new App({
	target: document.body,
	props: {
		// name: 'worldstarr',
	}
});

export default app;